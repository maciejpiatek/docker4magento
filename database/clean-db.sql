-- use your local url for the project

-- ACCORDING YOUR virtual host configuration
-- SET  @local_url PARAM PROPERLY!
set @local_url = 'http://<PROJECTNAME>.local/';
-- set @local_url = CONCAT('http://', database(), '.local/');
-- set @local_url = 'http://somename.lo/';

-- set default admin password

set @admin_id = (SELECT user_id FROM admin_user WHERE username = 'store_@dmin');
set @admin_passwd_hash = 'b90decfe52fc94b979cf6b77c1e329f9ad6071be83bcdf2130865e9d6355c346:UcQkWFWxLhw3MZmW4Rok8L0V5eKjfs76:1'; -- 123
DELETE FROM admin_passwords WHERE user_id = @admin_id;
UPDATE admin_user SET password=@admin_passwd_hash WHERE user_id = @admin_id;
UPDATE admin_user SET username='admin' WHERE user_id = @admin_id;
INSERT INTO admin_passwords (user_id, password_hash, expires) VALUES (@admin_id, @admin_passwd_hash, 0);


-- splitorders
UPDATE core_config_data SET value = 'postmaster@mg.convert.no' WHERE path = 'convert_split_orders/general/webshop_owner_email_identity';

-- Klarna
update core_config_data set value = '' where path = 'klarna/api/merchant_id';
update core_config_data set value = '' where path = 'klarna/api/shared_secret';


-- base url
UPDATE core_config_data SET value = @local_url WHERE path = 'web/unsecure/base_url';
UPDATE core_config_data SET value = NULL WHERE path = 'web/unsecure/base_link_url';
UPDATE core_config_data SET value = @local_url WHERE path = 'web/secure/base_url';
UPDATE core_config_data SET value = NULL WHERE path = 'web/secure/base_link_url';

-- cookie
update core_config_data set value = null where path = 'web/cookie/cookie_domain';

-- search engine
update core_config_data set value = 'mysql' where path = 'catalog/search/engine';

-- full page cache
UPDATE core_config_data SET value = '1' WHERE path = 'system/full_page_cache/caching_application';

-- GA, GTM, Weltpixel GTM
update core_config_data set value = 0 where path in ('google/analytics/active', 'googletagmanager/general/active', 'weltpixel_googletagmanager/general/enable', 'google/googletagmanager/active', 'fj_universalanalytics/general/enable');

-- order copy
update core_config_data set value = null where path in (
'checkout/payment_failed/copy_to',
'sales_email/order/copy_to'
);

-- mailgun from sportyme
update core_config_data set value = 'postmaster@mg.convert.no' where path = 'system/gmailsmtpapp/username';

-- FIXME: decode
update core_config_data set value = '0:2:H1uWkWJaKSzZ0HLBcjSMA0uCWKKNVChs:lx3s+bykIwjIdpqLIp1Uh3R3KvYkGBbjO3uFcRIn/3k=' where path = 'system/gmailsmtpapp/password';

-- disable magemonkey
update core_config_data set value = 0 where path = 'magemonkey/general/active';

-- algolia
update core_config_data set value = null where path = 'algoliasearch_credentials/credentials/api_key';
UPDATE core_config_data SET value = 'magento2_dev_test_' WHERE path = 'algoliasearch_credentials/credentials/index_prefix';

-- nosto
update core_config_data set value = null where path = 'nosto_tagging/installation/id';
update core_config_data set value = null where path = 'nosto_tagging/settings/account';
update core_config_data set value = null where path = 'nosto_tagging/settings/tokens';

-- clerk
update core_config_data set value = null where path = 'clerk/general/public_key';
update core_config_data set value = null where path = 'clerk/general/private_key';

-- flow
update core_config_data set value = 0 where path = 'convert_flow/general/enabled';

-- dotmailer
update core_config_data set value = 0 where path = 'connector_api_credentials/api/enabled';

-- product alert
delete from core_config_data where path in ('catalog/productalert/allow_price', 'catalog/productalert/allow_stock');

-- promo magento_reminder (EE)
update core_config_data set value = 0 where path = 'promo/magento_reminder/enabled';

update core_config_data set value = 0 where path = 'dev/static/sign';