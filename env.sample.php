<?php
return [
    'backend' =>
        [
            'frontName' => 'admin',
        ],
    'install' =>
        [
            'date' => 'Mon, 30 Jan 2017 14:27:36 +0000',
        ],
    'crypt' =>
        [
            'key' => 'bd3ec778ecf7d4fab2fbefe4f5893431',
        ],
    'session' =>
        [
            'save' => 'files',
        ],
    'db' =>
        [
            'table_prefix' => '',
            'connection' =>
                [
                    'default' =>
                        [
                            'host' => '<DBHOST>',
                            'dbname' => '<DBNAME>',
                            'username' => '<DBUSER>',
                            'password' => '<DBPASS>',
                            'model' => 'mysql4',
                            'engine' => 'innodb',
                            'initStatements' => 'SET NAMES utf8;',
                            'active' => '1',
                        ],
                ],
        ],
    'resource' =>
        [
            'default_setup' =>
                [
                    'connection' => 'default',
                ],
        ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'developer',
    'cache_types' =>
        [
            'config' => 1,
            'layout' => 1,
            'block_html' => 1,
            'collections' => 1,
            'reflection' => 1,
            'db_ddl' => 1,
            'eav' => 1,
            'customer_notification' => 1,
            'full_page' => 0,
            'config_integration' => 1,
            'config_integration_api' => 1,
            'translate' => 1,
            'config_webservice' => 1,
        ],
];
