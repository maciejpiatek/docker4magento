![Magento 2](https://cdn.rawgit.com/rafaelstz/magento2-snippets-visualstudio/master/images/icon.png)

#  Docker wrapper for Magento 2

**Linux:**

Install [Docker](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/) and [Docker-compose](https://docs.docker.com/compose/install/#install-compose).

### How to use

1\. Copy and configure .env and docker-compose.yml file
```bash
cp .env.sample .env

cp docker-compose.yml.sample docker-compose.yml

```

2\. Clone your project repository into application folder

3\. Download db dump and place it in database directory (is necessary to rename dump file to 'db.sql')

4\. Download and place your media static files into files directory

5\. Run ```bash ./init frontend magento-version``` (magento-version could be 22 or 21)

6\. Run ```bash sudo ./fix-permission``` - **need to improve script**

7\. Add entry of your project vhost in your host machine ```bash sudo vim /etc/hosts```

8\. Type on browser your vhost alias from 7 and enjoy your work!

### Features commands

| Commands  | Description  | Parameters / Example |
|---|---|---|
| `./init`  | Start/build containers, drop old database data (if exist) and import data (db.sql and clean-db.sql), composer install, optionaly build frontend (add parameters "./init frontend magento-version" ( magento-version can be 22 or 21)
| `./start`  | Start containers  | |
| `./stop`  | Stop your project containers  | |
| `./frontend`  |  Use Composer commands | 21 or 22 |
| `./fix-permission`  |  Use Composer commands | sudo ./fix-permission |
| `./import-db`  |  Import database dump from ./database directory |  |

### Credentials

- http://project-vhost.local/admin - admin/123
- http://127.0.0.102 (phpmyadmin) - root/123

### License

Inspiration from base repository: [clean-docker/Magento2](https://github.com/clean-docker/Magento2) 

MIT © 2019 